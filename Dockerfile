# syntax=docker/dockerfile:1.3
FROM python:3.9.11-slim-buster

RUN mkdir /app
WORKDIR /app

RUN pip install pipenv
COPY --chown=0:0 --chmod=755  Pipfile Pipfile
COPY --chown=0:0 --chmod=755  Pipfile.lock Pipfile.lock
RUN pipenv install --deploy --ignore-pipfile -v