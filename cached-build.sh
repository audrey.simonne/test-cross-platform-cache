# Run this with PLATFORM={arm64|x86}

echo "Printing docker version"
docker version


# echo "Cleaning out build cache between runs"
docker image prune -a
docker builder prune
docker system df

echo "Building image with x86 cache"
docker build --cache-from=asimval/build-cache-image:x86 -t final-image:${PLATFORM} --platform=linux/x86_64 .

# echo "Cleaning out build cache between runs"
docker image prune -a
docker builder prune
docker system df

echo "Building image with ARM64 cache"
docker build --cache-from=asimval/build-cache-image:arm64 -t final-image:${PLATFORM} --platform=linux/x86_64 .