# Run this with PLATFORM={arm64|x86} ./build.sh

echo "Printing docker version"
docker version

echo "Building image cross platform with cache export"
docker build --cache-to=type=inline -t asimval/build-cache-image:arm64 --platform=linux/x86_64 .

docker push asimval/build-cache-image:arm64